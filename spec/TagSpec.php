<?php

namespace spec\Jjanvier\TagPar;

use InvalidArgumentException;
use Jjanvier\TagPar\Tag;
use PhpSpec\ObjectBehavior;

class TagSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->beConstructedWith(4, 0, 19);
        $this->shouldHaveType(Tag::class);
    }

    function it_gives_major_minor_and_patch()
    {
        $this->beConstructedWith(4, 0, 19);

        $this->major()->shouldReturn(4);
        $this->minor()->shouldReturn(0);
        $this->patch()->shouldReturn(19);
    }

    function it_accepts_tags_beginning_by_v()
    {
        $this->beConstructedWith(4, 0, 19, true);

        $this->major()->shouldReturn(4);
        $this->minor()->shouldReturn(0);
        $this->patch()->shouldReturn(19);
    }

    function it_prints_a_tag()
    {
        $this->beConstructedWith(4, 0, 19);
        $this->__toString()->shouldReturn('4.0.19');
    }

    function it_prints_a_prefixed_tag()
    {
        $this->beConstructedWith(4, 0, 19, true);
        $this->__toString()->shouldReturn('v4.0.19');
    }

    function it_compares_to_the_same_tag()
    {
        $this->beConstructedWith(4, 0, 19);

        $this->compareTo('4.0.19')->shouldReturn(0);
    }

    function it_compares_to_a_previous_tag()
    {
        $this->beConstructedWith(4, 1, 19);

        $this->compareTo('3.6.87')->shouldReturn(1);
        $this->compareTo('4.0.87')->shouldReturn(1);
        $this->compareTo('4.1.14')->shouldReturn(1);
    }

    function it_compares_to_a_next_tag()
    {
        $this->beConstructedWith(4, 1, 19);

        $this->compareTo('5.0.4')->shouldReturn(-1);
        $this->compareTo('4.4.4')->shouldReturn(-1);
        $this->compareTo('4.1.66')->shouldReturn(-1);
    }

    function it_gives_next_patch()
    {
        $this->beConstructedWith(4, 1, 19);
        $this->nextPatch()->shouldReturn('4.1.20');
    }

    function it_gives_next_patch_when_prefixed_by_v()
    {
        $this->beConstructedWith(4, 1, 19, true);
        $this->nextPatch()->shouldReturn('v4.1.20');
    }

    function it_gives_next_minor()
    {
        $this->beConstructedWith(4, 1, 19);
        $this->nextMinor()->shouldReturn('4.2.0');
    }

    function it_gives_next_minor_when_prefixed_by_v()
    {
        $this->beConstructedWith(4, 1, 19, true);
        $this->nextMinor()->shouldReturn('v4.2.0');
    }

    function it_gives_next_major()
    {
        $this->beConstructedWith(4, 1, 19);
        $this->nextMajor()->shouldReturn('5.0.0');
    }

    function it_gives_next_major_when_prefixed_by_v()
    {
        $this->beConstructedWith(4, 1, 19, true);
        $this->nextMajor()->shouldReturn('v5.0.0');
    }

    function it_forbids_when_minor_or_patch_is_missing_from_a_string()
    {
        $this->beConstructedThrough('fromString', ['3']);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();

        $this->beConstructedThrough('fromString', ['3.2']);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_forbids_when_elements_are_not_integers_from_a_string()
    {
        $this->beConstructedThrough('fromString', ['foo.5.7']);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();

        $this->beConstructedThrough('fromString', ['3.bar.7']);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();

        $this->beConstructedThrough('fromString', ['3.5.baz']);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_forbids_negative_major()
    {
        $this->beConstructedWith(-3, 5, 7);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_forbids_negative_minor()
    {
        $this->beConstructedWith(3, -5, 7);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_forbids_negative_patch()
    {
        $this->beConstructedWith(3, 5, -7);
        $this->shouldThrow(InvalidArgumentException::class)->duringInstantiation();
    }

    function it_constructs_a_tag_with_a_string()
    {
        $this->beConstructedThrough('fromString', ['3.5.7']);

        $this->major()->shouldReturn(3);
        $this->minor()->shouldReturn(5);
        $this->patch()->shouldReturn(7);
    }

    function it_constructs_a_prefixed_tag_with_a_string()
    {
        $this->beConstructedThrough('fromString', ['v3.5.7']);

        $this->major()->shouldReturn(3);
        $this->minor()->shouldReturn(5);
        $this->patch()->shouldReturn(7);
    }
}
