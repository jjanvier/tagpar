<?php

namespace Jjanvier\TagPar;

use InvalidArgumentException;
use RuntimeException;

class Tag
{
    /** @var string */
    private $prefix;

    /** @var int */
    private $major;

    /** @var int */
    private $minor;

    /** @var int */
    private $patch;

    public function __construct(int $major, int $minor, int $patch, bool $withPrefix = false)
    {
        $prefix = $withPrefix ? 'v' : '';

        if ($major < 0 || $minor < 0 || $patch < 0) {
            throw new InvalidArgumentException('Inputs should be positive integers. Please check https://semver.org.');
        }

        $this->prefix = $prefix;
        $this->major = $major;
        $this->minor = $minor;
        $this->patch = $patch;
    }

    public static function fromString(string $rawTag): Tag
    {
        $parts = self::explodeInputTag($rawTag);

        return new self($parts['major'], $parts['minor'], $parts['patch'], !empty($parts['prefix']));
    }

    public function major(): int
    {
        return $this->major;
    }

    public function minor(): int
    {
        return $this->minor;
    }

    public function patch(): int
    {
        return $this->patch;
    }

    /**
     * Compare the current tag to a given string.
     *
     * @param string $rawOtherTag
     *
     * @return int 0 when tags are =, 1 when tag is > and -1 when tag is <
     */
    public function compareTo(string $rawOtherTag): int
    {
        $otherTag = self::fromString($rawOtherTag);

        if ($this->major() === $otherTag->major() &&
            $this->minor() === $otherTag->minor() &&
            $this->patch() === $otherTag->patch()) {
            return 0;
        }

        if ($this->major() > $otherTag->major()) {
            return 1;
        }

        if ($this->major() === $otherTag->major() &&
            $this->minor() > $otherTag->minor()) {
            return 1;
        }

        if ($this->major() === $otherTag->major() &&
            $this->minor() === $otherTag->minor() &&
            $this->patch() > $otherTag->patch()) {
            return 1;
        }

        if ($this->major() < $otherTag->major()) {
            return -1;
        }

        if ($this->major() === $otherTag->major() &&
            $this->minor() < $otherTag->minor()) {
            return -1;
        }

        if ($this->major() === $otherTag->major() &&
            $this->minor() === $otherTag->minor() &&
            $this->patch() < $otherTag->patch()) {
            return -1;
        }

        throw new RuntimeException('A comparison case was not handled properly...');
    }

    public function nextPatch(): string
    {
        return sprintf('%s%d.%d.%d', $this->prefix ?? '', $this->major(), $this->minor(), $this->patch() + 1);
    }

    public function nextMinor(): string
    {
        return sprintf('%s%d.%d.0', $this->prefix ?? '', $this->major(), $this->minor() + 1);
    }

    public function nextMajor(): string
    {
        return sprintf('%s%d.0.0', $this->prefix ?? '', $this->major() + 1);
    }

    /**
     * Check if an input is a valid tag like. See https://semver.org/#semantic-versioning-specification-semver.
     * The regexp used here is inspired from https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string.
     * Despite the fact that SemVer does not accept the "v" prefix (see https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string), TagPar accepts it for conveniency reasons.
     *
     * @param string $tag
     *
     * @return array
     */
    private static function explodeInputTag(string $tag): array
    {
        if (1 !== preg_match(
                '/^(?P<prefix>v?)(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*).*$/',
                $tag,
                $matches
            )) {
            throw new InvalidArgumentException('The input does not match a valid tag. Please check https://semver.org. Also, please not that for conveniency, contrary to SemVer, TagPar accepts the "v" prefix.');
        }

        return $matches;
    }

    public function __toString()
    {
        return sprintf('%s%d.%d.%d', $this->prefix ?? '', $this->major(), $this->minor(), $this->patch());
    }
}
